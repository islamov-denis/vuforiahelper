﻿using UnityEngine;
using Vuforia;

public class CustomTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{
    private TrackableBehaviour _trackableBehaviour;
    public TrackableBehaviour.Status Status { get; private set; }
    
    
    
    protected virtual void Start()
    {
        _trackableBehaviour = GetComponent<TrackableBehaviour>();
        if (_trackableBehaviour)
        {
            _trackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }
    
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
    {
        Status = newStatus;
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Trackable " + _trackableBehaviour.TrackableName + " found");
            OnTrackingFound();
        }
        
        else if ((previousStatus == TrackableBehaviour.Status.TRACKED ||
                  previousStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) &&
                  newStatus      == TrackableBehaviour.Status.NOT_FOUND)
        {
            Debug.Log("Trackable " + _trackableBehaviour.TrackableName + " lost");
            OnTrackingLost();
        }
        else
        {
            OnTrackingLost();
        }
    }
    
    protected virtual void OnTrackingFound()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents   = GetComponentsInChildren<Canvas>(true);

        foreach (var component in rendererComponents)
        {
            component.enabled = true;
        }

        foreach (var component in colliderComponents)
        {
            component.enabled = true;
        }

        foreach (var component in canvasComponents)
        {
            component.enabled = true;
        }
    }


    protected virtual void OnTrackingLost()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents   = GetComponentsInChildren<Canvas>(true);

        foreach (var component in rendererComponents)
        {
            component.enabled = false;
        }

        foreach (var component in colliderComponents)
        {
            component.enabled = false;
        }

        foreach (var component in canvasComponents)
        {
            component.enabled = false;
        }
    }
}