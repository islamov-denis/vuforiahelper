﻿using System;
using UnityEngine;
using Vuforia;

public class TransfromFilterForImageTarget : MonoBehaviour
{
    public CustomTrackableEventHandler TrackableEventHandler;
    
    [Space(10)]
    public Transform Anchor;
    public Transform Target;
    
    [Space(10)]
    public float TrackingLerp;
    public float TrackingSlerp;
    
    [Space(5)]
    public float ExtendTrackingLerp;
    public float ExtendTrackingSlerp;
    
    private void Update()
    {
        switch (TrackableEventHandler.Status)
        {
            case TrackableBehaviour.Status.TRACKED:
                TransformFiler(Target, Anchor, Time.deltaTime * TrackingLerp, Time.deltaTime * TrackingSlerp);
                break;
            case TrackableBehaviour.Status.EXTENDED_TRACKED:
                TransformFiler(Target, Anchor, Time.deltaTime * TrackingLerp, Time.deltaTime * ExtendTrackingSlerp);
                break;
        }
    }

    private static void TransformFiler(Component dst, Component src, float factor, float sfactor)
    {
        dst.transform.position = Vector3.Lerp(dst.transform.position, src.transform.position, factor);
        dst.transform.rotation = Quaternion.Slerp(dst.transform.rotation, src.transform.rotation, sfactor);
    }
}
